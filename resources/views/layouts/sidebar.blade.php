<div class="mdk-drawer  js-mdk-drawer"
                         id="default-drawer"
                         data-align="start">
                        <div class="mdk-drawer__content">
                            <div class="sidebar sidebar-light sidebar-left sidebar-p-t"
                                 data-perfect-scrollbar>
                                <div class="sidebar-heading" style="color:white;">Menu</div>
                                <ul class="sidebar-menu">
                                    <li class="sidebar-menu-item {{ in_array(Request::segment(2),array('brands')) ? 'open' : null }}">
                                        <a class="sidebar-menu-button"
                                           data-toggle="collapse"
                                           href="#pages_menu">
                                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">description</i>
                                            <span class="sidebar-menu-text">Pages</span>
                                            <span class="ml-auto sidebar-menu-toggle-icon"></span>
                                        </a>
                                        <ul class="sidebar-submenu collapse"
                                            id="pages_menu">
                                            <li class="sidebar-menu-item {{ in_array(Request::segment(2),array('brands')) ? 'active' : null }}">
                                                <a class="sidebar-menu-button"
                                                   href="{{ url('admin/brands') }}">
                                                    <span class="sidebar-menu-text">Brands</span>
                                                </a>
                                            </li>
                                            <li class="sidebar-menu-item">
                                                <a class="sidebar-menu-button"
                                                   href="#">
                                                    <span class="sidebar-menu-text">Cars</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="sidebar-heading" style="color:white;">Components</div>
                                <div class="sidebar-block p-0 mb-0">
                                </div>

                            </div>
                        </div>
                    </div>