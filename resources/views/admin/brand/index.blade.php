@extends('layouts.admin')
   
@section('content')
<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item">Pages</li>
                    <li class="breadcrumb-item active" aria-current="page">Brand</li>
                </ol>
            </nav>
            <h1 class="m-0">Brands</h1>
        </div>
        <a href="{{ route('brands.create') }}" class="btn btn-success ml-3">Create <i class="material-icons">add</i></a>
    </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <ul class="margin-bottom-none padding-left-lg">
        <li>{{ $message }}</li>
    </ul>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger">
    <ul class="margin-bottom-none padding-left-lg">
        <li>{{ $message }} </li>
    </ul>
</div>
@endif
<div class="row no-gutters" style="padding:25px;">
    <div class="col-lg-12 card-form__body">

        <table class="table mb-0 thead-border-top-0" id="datatable">
            <thead>
                <tr class="text-center">
                    <th>Brand name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($brands as $key => $brand)
            <tr>
                <td>{{ $brand->name }}</td>
                <td>{{ $brand->description }} </td>
                <td class="text-center">
                    <a class="btn btn-primary btn-sm" href="{{ route('brands.edit',$brand->id) }}"  title="Edit"><i class="fas fa-edit"></i></a>
                    <form method="post" action="{{ route('brands.destroy',$brand->id) }}" style='display:inline' >
                    @csrf
                    @method('DELETE')
                    <button type="submit"  onclick="return confirm('Are you sure to Delete the Brand?');" class="btn btn-danger btn-sm"  title="Delete" ><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection