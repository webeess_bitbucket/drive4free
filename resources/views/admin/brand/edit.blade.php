@extends('layouts.admin')
   
@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <ul class="margin-bottom-none padding-left-lg">
        <li>{{ $message }}</li>
    </ul>
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>
@endif
<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item">Pages</li>
                    <li class="breadcrumb-item" aria-current="page"><a href="{{ url('admin/brands') }}">Brand</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
            </nav>
            <h1 class="m-0">Edit Brand</h1>
        </div>
    </div>
</div>
<div class="row no-gutters" style="padding:25px;">
    <div class="col-lg-12 card-form__body card-body">
        <form method="post" action="{{ route('brands.update', $brand->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
            <div class="form-group">
                <label for="exampleInputEmail1">Name:</label>
                <input type="text" name="name" class="form-control" id="exampleInputEmail1" required value="{{$brand->name}}">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Description:</label>
                <textarea class="form-control" name="description" rows="10" id="description" required>{{$brand->description}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-warning" href="{{ url('admin/brands') }}">Back</a>
        </form>
    </div>
</div>
@endsection